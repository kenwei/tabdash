import os

from bottle import Bottle, run, static_file, response, request

from tabdash import TabDash

server = Bottle(autojson=False)
app = TabDash()


@server.route('/<filename:path>')
def serve(filename):
    root = os.path.join(app.root, '..', 'web')
    return static_file(filename, root)


@server.route('/api/lookup')
def lookup():
    """looks up the upstream and downstream dependencies of the give node.

    :params node: the name of the node

    :returns: A JSON response which will looks similar to:
              {upstream:   [['node0', 'node1'], [...]],
               downstream: [['node0', 'nodeA'], [...]]}
              the mark attribute is a placeholder for client application to
              identify the end of each chunk.
    """

    response.content_type = 'application/json'
    response.set_header('Cache-Control',
                        'public, max-age=43200')

    return app.get_edges(request.query.node)


@server.route('/api/traverse')
def graph():
    """traverse all nodes.

    :returns: A JSON response contains all the nodes and the corresponding
              links.
    """

    response.content_type = 'application/json'
    response.set_header('Cache-Control',
                        'public, max-age=43200')

    return app.get_graph()


@server.route('/api/resolve')
def resolve():
    """resolve all dependencies.

    :returns: A chunked response with all the resolved dependencies in JSON,
              which will looks similar to:
              {elements: {nodes: [{id:..., attr1:...}],
                          links: [{source:..., target:...}]},
               mark: 'eof'}
              the mark attribute is a placeholder for client application to
              identify the end of each chunk.
    """

    response.content_type = 'application/json'
    response.set_header('Cache-Control',
                        'public, max-age=43200')

    for graph in app.gen_graph():
        yield graph


run(server, **app.server_config)
