from ConfigParser import ConfigParser
import logging
import os
from os.path import join as op_join
try:
    import ujson as json
except:
    import json

from cachetools import TTLCache

from tabdash.graph.wrapper import DependencyGraphWrapper
from tabdash.resolver.resolver import DependencyResolver
from tabdash.utils.collection import convert_graph


class TabDash(object):

    """Main app of the project
    """

    def __init__(self):
        self._root = op_join(os.path.dirname(__file__))

        self._config = ConfigParser()
        self._config.read(op_join(self._root, '..', 'tabdash.cfg'))

        cache_ttl = self._config.getint('Cache', 'ttl')
        if  cache_ttl > 0 \
                and self._config.getint('Cache', 'size') > 0:
            self._cache = TTLCache(cache_ttl,
                                   ttl=self._config.getint('Cache', 'ttl'))
        else:
            self._cache = None

        logging.basicConfig(level=self._config.get('Resolver', 'logging'))

        self._resolver = DependencyResolver(self._config, cache=self._cache)
        self._wrapper = DependencyGraphWrapper(self._resolver,
                                               cache=self._cache)

    @property
    def root(self):
        """get project root

        :returns: project root path
        """
        return self._root

    @property
    def config(self):
        """get project config

        :returns: project configurations
        """
        return self._config

    @property
    def server_config(self):
        """get server configurations

        :returns: get API server related configurations
        """
        return {'server': self._config.get('Web Server', 'server'),
                'host': self._config.get('Web Server', 'host'),
                'port': self._config.getint('Web Server', 'port'),
                'debug': self._config.getboolean('Resolver', 'debug')}

    def gen_graph(self):
        """get dependency graph generator

        :returns: a generator of all dependency graph
        """
        for graphs in self._resolver.resolve():
            for graph in graphs:
                if graph:
                    yield json.dumps({'elements': convert_graph(graph),
                                      'mark': 'eof'})

    def get_graph(self):
        """get dependency graph

        :returns: all dependency graph
        """
        return json.dumps(self._wrapper.get_graph())

    def get_edges(self, node):
        """get upstream and downstream dependencies of the given node

        :param node: string, the node name

        :returns: upstream and downstream edges
        """
        return json.dumps({'upstream': self._wrapper.get_edges(node, False),
                           'downstream': self._wrapper.get_edges(node, True)})
