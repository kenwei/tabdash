import ast
from os.path import sep as op_sep, join as op_join, isfile as op_isfile

try:
    import ujson as json
except:
    import json

import networkx as nx

from tabdash.utils import Loggable
from tabdash.resolver import AbstractResolver
from tabdash.resolver.sql import DMLResolver


class DataMeerDependencyResolver(AbstractResolver):

    def _get_document_resolver(self, document):
        if (document.endswith('.json')):
            return ExportJobResolver()
        else:
            return WorkerScriptResolver()


class WorkerScriptResolver(Loggable):

    def resolve(self, file):
        connections = nx.DiGraph()
        path = file[:file.rfind(op_sep)]
        worker_src = ''.join(open(file).readlines())
        worker_ast = ast.parse(worker_src)

        tables = filter(lambda var: isinstance(var, ast.Assign) and
                        isinstance(var.targets[0], ast.Name) and
                        var.targets[0].id in ['table_list',
                                              'schema_list',
                                              'calculation_list'],
                        worker_ast.body)
        scripts = ['merge_v2'] if file.endswith('roi_worker.py') else []
        scripts = reduce(lambda c, l: c + l,
                         map(lambda var:
                             [v.s for v in (var.value.elts
                                            if isinstance(var.value, ast.List)
                                            else reduce(lambda c, l:
                                                        c + l.elts,
                                                        var.value.values, [])
                                            )],
                             tables), scripts)

        resolver = DMLResolver()
        weight_adjustments = dict()
        for s_name in scripts:
            if s_name.find('metric_ts_') < 0:
                s_path = op_join(path, 'sql', '{0}.sql'.format(s_name))
                if not op_isfile(s_path):
                    self.logger.debug('\t\t\tcorresponding script not found %s',
                                      s_path)
                    continue
                self.logger.debug(
                    '\t\tparsing corresponding script %s', s_path)
                s_src = ''.join(filter(lambda l: not l.startswith('\\')
                                       and not l.startswith('--'),
                                       open(s_path).readlines()))
                src_table = ('{0}_p2.{1}'.format(path[path.rfind(op_sep) + 1:],
                                                 s_name)).lower()
                if not s_name == 'merge_v2':
                    weight_adjustments[src_table] = 10
                    self.logger.debug(
                        '\t\t adjusting %s weighting...', src_table)
                depends = resolver.resolve(s_src)
                connections.add_nodes_from(depends.nodes(data=True))
                connections.add_edges_from(depends.edges())
        nx.set_node_attributes(connections, 'weight', weight_adjustments)
        return connections


class ExportJobResolver(Loggable):

    def resolve(self, file):
        connections = nx.DiGraph()
        job_src = json.loads(''.join(open(file).readlines()))
        job_def = job_src['file']['path'].split(op_sep)[-2:]
        job_name = op_join(*job_src['file']['path'].split(op_sep)[-2:]).lower()

        schema_name = job_def[0].replace('_etl', '_p2_stage')
        workbook_name = job_src['sheet']['workbook']['path'].split(
            op_sep)[-1:][0].replace('.wbk', '')
        sheet_name = job_src['sheet']['name']

        table_name = '{0}.{1}_stage'.format(schema_name, workbook_name).lower()
        connections.add_node(job_name, weight=1, category='job')
        connections.add_edge(job_name, table_name)
        table_name = '{0}.{1}'.format(job_def[0].replace('_etl', '_p2'),
                                      sheet_name).lower()
        connections.add_node(table_name, weight=10)
        connections.add_edge(job_name, table_name)

        return connections
