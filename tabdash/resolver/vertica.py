from tabdash.resolver import AbstractResolver
from tabdash.resolver.sql import DMLResolver


class PipelineDependencyResolver(AbstractResolver):

    BATCH_SIZE = 1024 * 10

    def _get_document_resolver(self, document):
        return DMLResolver()

    def _filter_comments(self, lines):
        return '\n'.join(filter(lambda l: not l.startswith('\\')
                                and not l.startswith('--')
                                and len(l.strip()) > 0,
                                lines))

    def _filter_uninterested(self, scripts):
        return ';'.join(filter(
            lambda s: not s.upper().startswith('COMMENT') and
            not s.upper().startswith('DROP') and
            not s.upper().startswith('CREATE FUNCTION') and
            not s.upper().startswith('CREATE SCHEMA') and
            not s.upper().startswith('CREATE SEQUENCE') and
            not s.upper().startswith('CREATE PROJECTION') and
            not s.upper().startswith('GRANT') and
            not s.upper().startswith('COMMIT') and
            not (s.startswith('CREATE TABLE')
                 and s.find('SELECT') < 0) and
            not s.upper().find('ADD CONSTRAINT') > -1,
            scripts))

    def _prepare_lines(self, lines):
        script = self._filter_comments(lines)
        return self._filter_uninterested(script.split(';'))

    def _preprocess_document(self, document):
        return self._prepare_lines(open(document).readlines())

    # @cachedmethod(operator.attrgetter('_cache'))
    # def _resolve_document(self, document):
    #     with exception_handling(self, document):
    #         resolver = self._get_resolver(document)
    #         # if os.stat(document).st_size > self.BATCH_SIZE:
    #         #     file = open(document)
    #         #     chunkBatch = ''
    #         #     chunk = file.readline()
    #         #     while chunk:
    #         #         chunkBatch += chunk
    #         #         chunk_end = chunk.rfind(';')
    #         #         if len(chunkBatch) > self.BATCH_SIZE and chunk_end > -1:
    #         #             script = self._prepare_lines(chunkBatch.split('\n'))
    #         #             if script:
    #         #                 self.logger.debug("\t\tprocess chunk size %d",
    #         #                                   len(script))
    #         #                 yield resolver.resolve(script)
    #         #             chunkBatch = ''
    #         #         chunk = file.readline()
    #         #     if len(chunkBatch) > 0:
    #         #         script = self._prepare_lines(chunkBatch.split('\n'))
    #         #         yield resolver.resolve(script)
    #         # else:
    #         script = self._prepare_lines(open(document).readlines())
    #         return resolver.resolve(script)
