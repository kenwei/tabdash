import itertools
try:
    import ujson as json
except:
    import json

from concurrent.futures import ThreadPoolExecutor

from tabdash.utils import Loggable
from tabdash.resolver.vertica import PipelineDependencyResolver
from tabdash.resolver.datameer import DataMeerDependencyResolver
from tabdash.resolver.tableau import TableauDependencyResolver


class DependencyResolver(Loggable):

    def __init__(self, config, cache=None):
        super(DependencyResolver, self).__init__()

        self._config = config
        self._cache = cache

    def _is_debug(self):
        return self._config.getboolean('Resolver', 'debug')

    def _get_doc_roots(self):
        roots = set()
        for resolver_section in filter(lambda s: s.startswith('Resolver/'),
                                       self._config.sections()):
            roots.add(self._config.get(resolver_section, 'root'))
        return roots

    def _refresh_docroot(self):
        if self._config.getboolean('Resolver', 'refresh'):
            from git import Repo
            from PyGitUp.git_wrapper import GitWrapper
            for root in self._get_doc_roots():
                repo = GitWrapper(Repo(root))
                self.logger.debug("Refreshing %s...", root)
                repo.checkout('master')
                self.logger.debug("... %s", repo.run('pull', '--rebase'))

    def _get_resolver_options(self, section):
        options = {}
        section_key = 'Resolver/%s' % section
        for option_key in self._config.options(section_key):
            options[option_key] = self._config.get(section_key, option_key)
        return options

    def _get_resolves(self):
        if not self._is_debug():
            executor = ThreadPoolExecutor(
                max_workers=self._config.get('Resolver', 'workers')).map
        else:
            executor = map

        return itertools.chain(
            TableauDependencyResolver(self._get_resolver_options('Tableau'),
                                      cache=self._cache)
            .resolve(executor=executor),
            PipelineDependencyResolver(self._get_resolver_options('Vertica'),
                                       cache=self._cache)
            .resolve(executor=executor),
            DataMeerDependencyResolver(self._get_resolver_options('Datameer'),
                                       cache=self._cache)
            .resolve(executor=executor),
        )

    def resolve(self):
        self._refresh_docroot()
        self.logger.debug("preparing resolvers...")
        resolvers = self._get_resolves()
        self.logger.debug("resolving document...")
        return resolvers
