import networkx as nx
import sqlparse
from sqlparse.sql import Parenthesis
from sqlparse.tokens import Keyword, Name, Punctuation, DML, DDL

from tabdash.utils import Loggable
from tabdash.utils.helper import weight_table


class QueryResolver(Loggable):

    """Query Resolver, a resolver to identify the tables used in
    query statements.
    """

    def _token_walker(self, tokens, skip_punc=True):
        for token in tokens:
            if (token.is_whitespace() or (skip_punc and
                                          token.ttype == Punctuation)):
                continue
            else:
                yield token

    def _resolve_table_name(self, tokens):
        idx = next((i for i, v in enumerate(tokens) if v == '.'), -1)
        if idx > 0:
            return ''.join(tokens[idx - 1 : idx + 2]).lower()
        else:
            return tokens[idx + 1]

    def _resolve_statement(self, tokens):
        tables = set()
        pre_item = None
        tbl_name = list()
        for token in tokens:
            if pre_item is not None:
                if token.ttype == Name:
                    tbl_name.append(token.value.strip().lower())
                    continue
                elif token.ttype == Parenthesis:
                    continue
                elif token.ttype == Punctuation:
                    if token.value == ',':
                        tables.add(self._resolve_table_name(tbl_name))
                        del tbl_name[:]
                    if token.value == '.':
                        tbl_name.append(token.value)
                else:
                    if tbl_name:
                        tables.add(self._resolve_table_name(tbl_name))
                        del tbl_name[:]
                    pre_item = None
            if token.ttype == Keyword:
                if (token.value.upper() == 'FROM' or
                        token.value.upper().__contains__('JOIN')):
                    pre_item = token
        if tbl_name:
            tables.add(self._resolve_table_name(tbl_name))
        return tables

    def resolve(self, script):
        """resolve the given script.

        :param script: a string containing the query statements

        :returns: a set of tables used in the given script
        """

        tables = set()
        for statement in sqlparse.parse(script):
            tokens = self._token_walker(statement.flatten(),
                                        skip_punc=False)
            for table in self._resolve_statement(tokens):
                tables.add(table)

        return tables


class DMLResolver(QueryResolver):

    """DML Resolver, a resolver to identify the tables dependencies in DML
    statements.
    """

    def __init__(self, weighter=weight_table):
        super(DMLResolver, self).__init__()
        self._weight = weighter

    def _match_token(self, token, ttype, value):
        return token and token.ttype == ttype and \
            token.value.upper() in value

    def _next(self, tokens):
        token = None
        try:
            token = next(tokens)
        except StopIteration:
            pass
        return token

    def _first(self, tokens, ttype, value=None):
        token = self._next(tokens)
        while (token and
               (not token.ttype == ttype or
                (value and not token.value.upper() in value))):
            token = self._next(tokens)
        return token

    def _either(self, tokens, value=None):
        token = self._next(tokens)
        while (token and
                (value and not token.value.upper() in value)):
            token = self._next(tokens)
        return value[value.index(token.value.upper())] if token else None

    def _extract_name(self, token, delimiter=' ', strip=False, first=True):
        name = ''.join(map(lambda t: (t.value.strip()
                                      if strip else t.value).lower(),
                           token.flatten())) if token else ''
        if name.find('(') > 0 and name.find(')') > 0:
            name = name.split('(')[0]
        name_split = name.split(delimiter)
        return name_split[0] if first else name_split

    def resolve(self, script):
        """Resolves the given DML script.

        Note: For table or schema renames, this resolver will add a 'renamed_from'
        attribute to the renamed tables as an identification.

        :param script: a string containing the DML statements

        :returns: a NetworkX graph data structure containing all the tables (as
                  nodes) and the relationships among them (as edges)
        """
        connections = nx.DiGraph()
        for statement in sqlparse.parse(script):
            tokens = self._token_walker(statement.tokens)
            token = self._next(tokens)
            if self._match_token(token, DML, 'MERGE'):
                target = None
                if self._first(tokens, Keyword, 'INTO'):
                    token = self._next(tokens)
                    target = self._extract_name(token)
                if target and self._first(tokens, Keyword, 'USING'):
                    self._weight(target, connections)
                    token = next(tokens)
                    source = self._extract_name(token)
                    self._weight(source, connections)
                    connections.add_edge(source, target)

            elif self._match_token(token, DML, 'UPDATE'):
                token = self._next(tokens)
                target = self._extract_name(token)
                if target and self._first(tokens, Keyword, 'FROM'):
                    self._weight(target, connections)
                    token = self._next(tokens)
                    source = self._extract_name(token)
                    self._weight(source, connections)
                    connections.add_edge(source, target)

            elif self._match_token(token, DML, 'INSERT'):
                if self._first(tokens, Keyword, 'INTO'):
                    token = self._next(tokens)
                    target = self._extract_name(token, strip=True)
                    tables = self._resolve_statement(
                        self._token_walker(statement.flatten(),
                                           skip_punc=False))
                    if tables:
                        self._weight(target, connections)
                        for table in tables:
                            self._weight(table, connections)
                            connections.add_edge(table, target)

            elif self._match_token(token, DDL, ['CREATE', 'CREATE OR REPLACE']):
                if self._first(tokens, Keyword, ['TABLE', 'VIEW']):
                    token = self._next(tokens)
                    target = self._extract_name(token).strip()
                    tables = self._resolve_statement(
                        self._token_walker(statement.flatten(),
                                           skip_punc=False))
                    if tables:
                        self._weight(target, connections)
                        for table in tables:
                            self._weight(table, connections)
                            connections.add_edge(table, target)

            elif self._match_token(token, DDL, 'ALTER'):
                if self._first(tokens, Keyword, 'TABLE'):
                    token = self._next(tokens)
                    from_names = self._extract_name(
                        token, delimiter=',', strip=True, first=False)
                    renames = dict()
                    alter_type = self._either(tokens, ['RENAME', 'SET'])
                    if alter_type == 'RENAME' and \
                            self._first(tokens, Keyword, 'TO'):
                        token = self._next(tokens)
                        for idx, to_name in enumerate(self._extract_name(
                                token, delimiter=',', strip=True, first=False)):
                            if not to_name.__contains__('.'):
                                schema, name = from_names[idx].split('.')
                                to_name = "{0}.{1}".format(schema, to_name)
                            self._weight(to_name, connections)
                            self._weight(from_names[idx], connections)
                            renames[to_name] = from_names[idx]
                    elif alter_type == 'SET' \
                            and self._first(tokens, Keyword, 'SCHEMA'):
                        token = self._next(tokens)
                        for idx, to_schema in enumerate(self._extract_name(
                                token, delimiter=',', strip=True, first=False)):
                            from_schema, name = from_names[idx].split('.')
                            to_name = "{0}.{1}".format(to_schema, name)
                            self._weight(to_name, connections)
                            self._weight(from_names[idx], connections)
                            renames[to_name] = from_names[idx]
                    if renames:
                        nx.set_node_attributes(connections,
                                               'renamed_from',
                                               renames)
        return connections
