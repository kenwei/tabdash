import operator

try:
    import ujson as json
except:
    import json

from cachetools import cachedmethod

from tabdash.utils import Cachable
from tabdash.utils import io
from tabdash.utils.helper import exception_handling


class AbstractResolver(Cachable):

    def __init__(self, config, cache=None):
        super(AbstractResolver, self).__init__(cache=cache)
        self._root = config.get('root')
        self._files = map(lambda f: f.strip(), config.get('files').split(','))

    def _get_document_resolver(self, document):
        pass

    def _preprocess_document(self, document):
        return document

    @cachedmethod(operator.attrgetter('_cache'))
    def _resolve(self, document):
        with exception_handling(self, document):
            document_resolver = self._get_document_resolver(document)
            return document_resolver.resolve(
                self._preprocess_document(document))

    def resolve(self, executor=map):
        documents = io.file_walker(self._root, *self._files)

        yield executor(self._resolve, documents)
