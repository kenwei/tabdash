from os.path import sep as op_sep
from defusedxml.ElementTree import parse
from HTMLParser import HTMLParser

import networkx as nx

from tabdash.utils import Loggable
from tabdash.resolver import AbstractResolver
from tabdash.resolver.sql import QueryResolver
from tabdash.utils.helper import weight_table


class TableauDependencyResolver(AbstractResolver):

    def _get_document_resolver(self, document):
        identities = document.split(op_sep)[-3:]
        if (identities[0] == 'extract'):
            return DataSourceWorkbookResolver()
        else:
            return ReportWorkbookResolver()


class DataSourceWorkbookResolver(Loggable):

    def __init__(self):
        super(DataSourceWorkbookResolver, self).__init__()
        self.h_parser = HTMLParser()
        self.s_resolver = QueryResolver()

    def _parse_workbook(self, document):
        doc_id = '/'.join(document.split(op_sep)[-3:][1:])
        workbook = parse(open(document)).getroot()
        return (doc_id, workbook)

    def _resolve_table_dependencies(self, wbname, workbook):
        connections = nx.DiGraph()
        for datasource in workbook.findall('./datasources/datasource'):
            dsname = datasource.attrib.get('caption') \
                if (datasource.attrib.__contains__('caption')) \
                else datasource.attrib.get('name')
            if (dsname and not dsname == 'Parameters'):
                prj_name = wbname.split('/')[0]
                ds_fullname = '%s/%s' % (prj_name, dsname)
                connections.add_node(ds_fullname, weight=100
                                     if prj_name == 'marketing'
                                     else 110,
                                     category='datasource')
                for relation in datasource.findall('./connection//relation'):
                    r_type = relation.attrib.get('type')
                    r_name = relation.attrib.get('name')
                    if (r_type == 'text'):
                        r_tables = self.s_resolver.resolve(
                            self.h_parser.unescape(relation.text))
                        for r_table in r_tables:
                            weight_table(r_table, connections)
                            connections.add_edge(r_table, ds_fullname)
                    else:
                        if (r_type == 'table' and not r_name == 'sqlproxy'):
                            r_table = relation.attrib.get('table')
                            if (r_table):
                                tbl_name = r_table.replace('[', '')\
                                                  .replace(']', '')\
                                                  .strip().lower()
                                weight_table(tbl_name, connections)
                                connections.add_edge(tbl_name,
                                                     ds_fullname)
        return connections

    def resolve(self, document):
        # extract
        wbname, workbook = self._parse_workbook(document)
        connections = self._resolve_table_dependencies(wbname, workbook)
        connections.name = document
        return connections


class ReportWorkbookResolver(DataSourceWorkbookResolver):

    def _resolve_wb_name(self, workbook):
        wb_repo = workbook.find('./repository-location')
        if not wb_repo == None:
            attr = wb_repo.attrib
            wbname = attr.get('derived-from').split('/')[-1:][0] \
                if attr.__contains__('derived-from') \
                else attr.get('id')
            wbname = wbname.split('?')[0] \
                if wbname.__contains__('?') \
                else wbname
            return wbname

        return "unknown"

    def resolve(self, document):
        # workbook
        wbname, workbook = self._parse_workbook(document)
        connections = nx.DiGraph(name=document)
        lives = self._resolve_table_dependencies(wbname, workbook)

        for ws in workbook.findall('*/worksheet'):
            wsname = ws.attrib.get('name')
            if (wsname):
                prj_name = wbname.split('/')[0]
                ws_fullname = "{0}/{1}".format(wbname.replace(
                    prj_name + '/', ''),
                    wsname)
                connections.add_node(ws_fullname,
                                     weight=120,
                                     category='report',
                                     parent=wbname)
                self.logger.debug('\tparsing worksheet %s', ws_fullname)
                datasources = ws.findall('*//datasource')
                for ds in datasources:
                    dsname = ds.attrib.get('caption') \
                        if (ds.attrib.__contains__('caption')) \
                        else ds.attrib.get('name')
                    if (dsname == 'Parameters'):
                        continue
                    if (lives and lives.__contains__(dsname)):
                        for table in lives.get(dsname):
                            weight_table(table, connections)
                            connections.add_edge(table, ws_fullname)
                    else:
                        ds_fullname = '%s/%s' % (prj_name, dsname)
                        connections.add_node(ds_fullname,
                                             weight=100
                                             if prj_name == 'marketing'
                                             else 110,
                                             category='datasource')
                        connections.add_edge(ds_fullname, ws_fullname)
        return connections
