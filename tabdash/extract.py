import click
import os
from tabdash import TabDash


@click.command()
@click.option('--file', help='The output file name')
def extract(file):
    """Extract the whole graph and output as JSON."""
    app = TabDash()

    output = file if file else os.path.join(
        app.root, '..', 'orientdb', 'data.json')
    with open(output, 'w') as writer:
        writer.write(app.get_graph())

if __name__ == '__main__':
    extract()