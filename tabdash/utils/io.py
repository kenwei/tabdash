from fnmatch import fnmatch
import os


def file_walker(folder, *pattern):
    for root, dirnames, filenames in os.walk(folder):
        for file in filter(lambda f: any(fnmatch(f, p) for p in pattern),
                           map(lambda f: os.path.join(root, f), filenames)):
            yield file
