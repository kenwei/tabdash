import logging


class Loggable(object):

    def __init__(self):
        self.logger = logging.getLogger(__name__)


class Cachable(Loggable):

    def __init__(self, cache=None):
        super(Cachable, self).__init__()
        self._cache = cache
