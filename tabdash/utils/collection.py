def node_tuple_to_map(tuple):
    node = {'name': tuple[0]}
    node.update(tuple[1])
    return node


def link_tuple_to_map(tuple):
    return {'source': tuple[0], 'target': tuple[1]}


def convert_graph(graph):
    return {'nodes': map(lambda node: node_tuple_to_map(node),
                         graph.nodes(data=True)),
            'links': map(lambda link: link_tuple_to_map(link),
                         graph.edges())}
