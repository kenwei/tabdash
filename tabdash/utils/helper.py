from contextlib import contextmanager


@contextmanager
def exception_handling(loggable, thing):
    try:
        loggable.logger.info("handling %s...", thing)
        yield thing
    except Exception as e:
        loggable.logger.warning("\tfailed due to %s", e)
        import sys
        import traceback
        et, ev, etb = sys.exc_info()
        loggable.logger.debug("\tstack trace as %s", traceback.format_tb(etb))
        pass


def weight_table(target, connections, weight=30):
    if target.find('stage_') > -1 or \
       target.find('_stage') > -1 or \
       target.find('_stale') > -1 or \
       target.find('temp') > -1 or \
       target.find('analysis_export.') > -1:
        connections.add_node(target)
    elif target.find('roi_p2.v_user_first') > -1:
        connections.add_node(target, weight=weight - 10)
    elif target.find('roi_p2.v_') > -1 or \
            target.find('datacube.') > -1:
        connections.add_node(target, weight=weight + 40)
    elif target.find('_v2') > -1:
        connections.add_node(target, weight=weight + 30)
    elif (target.find('roi_p2.') > -1 or
          target.find('analytics_p2.') > -1):
        connections.add_node(target, weight=weight)
    elif target.find('mk_tableau.dw') > -1:
        connections.add_node(target, weight=weight + 20)
    else:
        connections.add_node(target)
