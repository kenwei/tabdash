from collections import defaultdict

try:
    import ujson as json
except:
    import json

import networkx as nx

from tabdash.utils import Loggable

CACHE_KEY = 'dendrogram'


class DependencyGraphWrapper(Loggable):

    """Dependency Graph Wrapper, a wrapper of dependency graph which provides
    some function for node to node dependency inquiry.
    """

    def __init__(self, resolver, cache=None):
        super(DependencyGraphWrapper, self).__init__()
        self._resolver = resolver
        self._cache = cache
        self._graph = None

    def _get_graph(self):
        if self._graph:
            return self._graph

        if self._cache is not None and self._cache.__contains__(CACHE_KEY):
            self._graph = self._cache[CACHE_KEY]
        else:
            self._graph = nx.DiGraph()
            for graphs in self._resolver.resolve():
                for graph in graphs:
                    if graph:
                        self._graph.add_nodes_from(graph.nodes(data=True))
                        self._graph.add_edges_from(graph.edges())
            self._cache[CACHE_KEY] = self._graph

        self._handling_renames()
        self._weighting_nodes()

        return self._graph

    def _weighting_nodes(self):
        weighted_nodes = nx.get_node_attributes(self._graph, 'weight')
        self._nodes = defaultdict(set)
        for node, weight in weighted_nodes.iteritems():
            self._nodes[weight].add(node)
        self._levels = sorted(self._nodes.keys())

    def _handling_renames(self):
        # handling table renames
        r_graph = self._graph.reverse()
        for target, source in nx.get_node_attributes(self._graph,
                                                     'renamed_from')\
                .iteritems():
            for edge in self._graph.edges(source):
                self._graph.add_edge(target, edge[1])

            for edge in r_graph.edges(source):
                self._graph.add_edge(edge[1], target)

    def get_graph(self):
        nodes = dict()
        graph = self._get_graph()
        for node in graph.nodes(data=True):
            nodes[node[0]] = node[1]
            nodes[node[0]].update({'name': node[0], 'links': []})
        for edge in graph.edges():
            nodes[edge[0]]['links'].append(edge[1])
        return nodes.values()

    def get_edges(self, node_name, downward=True):
        result = []
        graph = self._get_graph() if downward else self._get_graph().reverse()
        level = len(self._levels) - 1 if downward else 0
        for edge_name in self._nodes[self._levels[level]]:
            if nx.has_path(graph, node_name, edge_name):
                result.append(nx.shortest_path(graph, node_name, edge_name))
        return result
