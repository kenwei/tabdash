var ui = {};

ui.monitoring = function(options) {
    var _monitoring = {
        init: function(options) {
            this._options = _.extend(options ? options : {}, {
                cache: {
                    ttl: 720,
                    key: 'dendrogram'
                },
                parser: {
                    endpoint: 'api/resolve',
                    eocf: "\"mark\":\"eof\"}"
                }
            });
            this._dendrogram = charts.dendrogram();
            return this;
        },

        start: function() {
            var self = this;
            var cache = lscache.get(self._options.cache.key);
            if (cache) {
                console.log("fetching data from local cache...");
                self._loadDataChunk(0, cache);
                self._prepareUI();
            } else {
                console.log("fetching data from remote server...");
                var marker = 0;
                $.ajax({
                    url: self._options.parser.endpoint,
                    dataType: 'json',
                    xhrFields : {
                        onreadystatechange: function(e) {
                            if (4 == e.target.readyState) {
                                lscache.set(self._options.cache.key,
                                            e.target.response,
                                            self._options.cache.ttl);
                                console.log("data loading completed...");
                                self._loadDataChunk(marker,
                                                    e.target.response);
                                self._prepareUI();
                            }
                        },
                        onprogress: function(e) {
                            if (4 != e.target.readyState) {
                                marker = self._loadDataChunk(marker,
                                                             e.target.response);
                            }
                        }
                    }
                });
            }
        },

        _loadDataChunk: function(marker, chunk) {
            var tail = chunk.indexOf(this._options.parser.eocf, marker);
            while (tail > 0) {
                tail += this._options.parser.eocf.length;
                if (marker != tail) {
                    var serialized = chunk.substring(marker, tail);
                    try {
                        var nodeCounts = this._dendrogram.load(JSON.parse(serialized).elements);
                        $('div.dimmer div.text').text(
                            nodeCounts + ' nodes processed...')
                    } catch (ex) {
                        console.log(ex);
                    }
                }
                marker = tail;
                tail = chunk.indexOf(this._options.parser.eocf, marker);
            }
            return marker;
        },

        _prepareUI: function() {
            var self = this;
            $('div.dimmer div.text').text('Optimizing element dependencies...')
            self._dendrogram.optimize();

            $('div.pipeline.settings.button').bind("click", function() {
                $('div.pipeline.control').sidebar('setting', 'transition', 'scale down').sidebar('toggle');
            });
            $('div.report.settings.button').bind("click", function() {
                $('div.report.control').sidebar('setting', 'transition', 'scale down').sidebar('toggle');
            });

            var elements = self._dendrogram.data('elements');
            this._prepareFilter('pipeline', elements[0], charts.enum.DOWNWARD);
            this._prepareFilter('report', _.last(elements).filter(function(e) {
                return e['flattened'] === false;
            }), charts.enum.UPWARD);

            $('.ui.checkbox').checkbox();
            $('.check.button').click();
            $('.ui.search').search({
                source: self._dendrogram.data('flattened_elements').map(function(e) {
                    return {'title': e.name};
                }),
                onSelect: function(e) {
                    self._dendrogram.filter([e.title], charts.enum.BIDIRECTION);
                }
            });

            $('div.dimmer').removeClass('active').addClass('disabled');
        },

        _prepareFilter: function(type, elements, direction) {
            var self = this;
            $('div.' + type + '.settings.button').bind("click", function() {
                $('div.' + type + '.control').sidebar('setting', 'transition', 'scale down').sidebar('toggle');
            });
            elements.sort(function(e1, e2) {
                return d3.ascending(e1.name, e2.name);
            }).forEach(function(element) {
                $('div.' + type + '.filters').append(
                    $('<div></div>')
                        .addClass('ui toggle checkbox inverted')
                        .append($('<input></input>').attr('value', element.name)
                                                    .attr('type', 'checkbox'))
                        .append($('<label></label>').text(element.name))
                );
            });
            $('div.' + type + '.filters .checkbox').checkbox('attach events', 'div.' + type + ' .check.button', 'check');
            $('div.' + type + '.filters .checkbox').checkbox('attach events', 'div.' + type + ' .uncheck.button', 'uncheck');
            $('div.' + type + '.control .apply.button').on('click', function(e) {
                var filtered = [];
                if ($('div.' + type + '.filters .checkbox.checked input').length < $('div.' + type + '.filters .checkbox input').length) {
                    $.each($('div.' + type + '.filters .checkbox.checked input'),
                           function(i, c) { filtered = filtered.concat($(c).val()); });
                }
                self._dendrogram.filter(filtered, direction);
            });
        }
    };

    return _monitoring.init(options);
};