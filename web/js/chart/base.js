var charts = {};

charts.utils = {
    /**
     * charts.utils - lib of utility functions
     */

    mapObject: function(sourceObjects, targetObjects, attr) {
        /* mapping the attr property of each object contained in
           targetObjects with the string contained in sourceObjects */

        return sourceObjects ? sourceObjects.map(function(c) {
            return targetObjects.filter(function(n) { return n[attr] === c; })[0];
        }).filter(function(e) { return e; }) : [];
    },

    escape: function(str) {
        /* to escape the given string */

        return str ? str.replace(/[\s+\(\)\/\.%\>\<\'\"\&>]/g, '_') : '';
    },
};

charts.enum = {
    DOWNWARD: -1,
    BIDIRECTION: 0,
    UPWARD: 1
};

charts.data = {
    /**
     * charts.data - lib of data manipulation functions
     */

    nodekey: function(node) {
        /* get the key of the node */

        return node.class + '_' + charts.utils.escape(node.name);
    },

    linkkey: function(link) {
        /* get the key of the link */

        return charts.data.nodekey(link.source) + "_to_" +
               charts.data.nodekey(link.target);
    },

    cascade: function(name, dependencies, downward) {
        /* return all downward or upward children elements */

        var self = this;
        var result = [];
        var to_retrieve = [name];
        while (to_retrieve.length > 0) {
            var temp = [];
            to_retrieve.forEach(function(n) {
                temp = temp.concat(dependencies.filter(function(l) {
                    return (n === (downward ? l.source : l.target)) &&
                           result.indexOf(n) < 0;
                })
                .map(function(l) {
                    return downward ? l.target : l.source;
                }));
            });
            result = _.uniq(result.concat(to_retrieve));
            to_retrieve = _.uniq(temp);
        }
        return result;
    },

    layout: function(elements, dependencies, dimensions, flattened) {
        /* layout the given elements accordingly */

        var dy = 250;
        var nodes = [];
        for (var l0 = elements.length - 1; l0 >= 0; l0--) {
            var rightEnd = (l0 == elements.length - 1);
            var levelElements = elements[l0];
            if (rightEnd) {
                levelElements = levelElements.filter(function (e) {
                    return e.flattened === flattened;
                });
            }
            var dx = 35;
            nodes = nodes.concat(levelElements.sort(function(e1, e2) {
                return d3.ascending(e1.name, e2.name);
            }).map(function(e, i) {
                var names = (e.name.indexOf('/') > 0 ?
                             e.name.replace('/', '=') :
                             e.name.replace('.', '=')).split('=');
                return {
                    "name": e.name,
                    "category": names[0],
                    "class": e.class,
                    "displayName": names[names.length - 1],
                    "y": l0 * dy,
                    "x": 25 + i * dx,
                    "leftEnd": l0 == 0,
                    "rightEnd": rightEnd
                };
            }));
        }
        nodes.forEach(function(node) {
            if (!node.rightEnd) {
                children_names = dependencies.filter(function(d) {
                    return d.source === node.name;
                }).map(function(d) { return d.target; });
                node.children = charts.utils.mapObject(
                                children_names, nodes, 'name');
            }
            node.weight = node.children ? node.children.length : 0;
        });
        return nodes;
    },

    unflatten: function(names, elements) {
        /* unflatten element names */

        elements.filter(function(node) {
            return node.flattened === false;
        }).forEach(function(node) {
            var idx = names.indexOf(node.name);
            if (idx > -1) {
                parentName = names[idx];
                names.splice(idx, 1);
                names = names.concat(elements.filter(function(e) {
                    return e.parent === parentName;
                }).map(function(e) {
                    return e.name;
                }));
            }
        });
        return names;
    },

    filter: function(targets, elements, dependencies, direction) {
        /* filter the dependencies of the given targets */

        var filtered = [];
        var flattenables = elements[elements.length - 1];
        charts.data.unflatten(targets, flattenables).forEach(function(name) {
            var cascaded;
            if (direction === charts.enum.BIDIRECTION) {
                cascaded = _.uniq(charts.data.cascade(name, dependencies, true).concat(
                    charts.data.cascade(name, dependencies, false)
                ));
            } else {
                cascaded = charts.data.cascade(name,
                                               dependencies,
                                               direction === charts.enum.DOWNWARD);
            }

            elements.forEach(function(level, index) {
                var matched = level.filter(function (element) {
                    return cascaded.indexOf(element.name) > -1;
                });
                if (filtered.length > index) {
                    filtered[index] = _.uniq(filtered[index].concat(matched));
                } else {
                    filtered[filtered.length] = matched;
                }
            });
        });

        return filtered;

    },
}

charts.graph = {
    /**
     * charts.graph - lib of graph manipulation functions
     */

    map: function(graph, attribute) {
        /* get a map of the mapping of node name and the value of the given attribute */

        var n2a = d3.map();
        graph.nodes(true)
             .filter(function(node) {
                 return node[1][attribute] !== undefined;
             }).forEach(function(node) {
                 return n2a.set(node[1].name, node[1][attribute]);
             });
        return n2a;
    },

    group: function(graph, attribute) {
        /* group all the nodes of the graph by the value of the given attribute */

        return d3.nest().key(function(n) {
                             return n[attribute];
                        })
                        .sortKeys(d3.ascending)
                        .map(
                            graph.nodes(true).filter(function(node) {
                                return node[1][attribute] !== undefined;
                            }).map(function(node) {
                                return node[1];
                            }),
                            d3.map
                        );
    },

    traverse: function(container, sources, graph, usedNodes, parentNodes, pathFilter) {
        /* traverse the graph and have all edges stored in the given container */

        sources.forEach(function(source) {
            jsnx.singleSourceShortestPath(graph, source.name)
                .forEach(function(s_path) {
                    var path = pathFilter(s_path);
                    if (path.length > 1) {
                        _.zip(path, _.rest(path)).forEach(function(edge) {
                            usedNodes.set(edge[0], 1);
                            usedNodes.set(edge[1], 1);
                            container.set(edge[0] + '_' + edge[1], {'source': edge[0], 'target': edge[1]});
                            if (parentNodes.has(edge[1])) {
                                container.set(edge[0] + '_' + parentNodes.get(edge[1]),
                                              {'source': edge[0], 'target': parentNodes.get(edge[1])});
                            }
                        })
                    }
                });
        });
    }

};