charts.dendrogram = function(options) {
    /**
     * charts.dendrogram - dendrogram builder
     */

    var _dendrogram = {

        updateNodes: function(name, value) {
            /* to update nodes by name */

            var self = this;
            return function(d) {
                var node_id = "#node-" + charts.data.nodekey(d[name]);
                self._d3.svg.select(node_id).classed(name, value);
            };
        },

        init: function(options) {
            /* to initiate the dendrogram */

            this._options = _.extend(options ? options : {}, {
                flattened: false,
                container: '#container',
                dimensions: {
                    width: $(window).width() - 50,
                    height: $(window).height() - 50,
                    margin: {
                        left: 250,
                        right: 800
                    }
                }
            });
            this._d3 = {};
            this._d3.cluster = d3.layout.cluster()
                                 .size([this._options.dimensions.height,
                                        this._options.dimensions.width -
                                        this._options.dimensions.margin.right]);
            this._d3.diagonal = d3.svg.diagonal()
                                  .projection(function(d) {
                                      return [d.y, d.x];
                                  });
            this._d3.svg = d3.select(this._options.container).append("svg")
                             .attr("width", this._options.dimensions.width)
                             .attr("height", this._options.dimensions.height)
                             .append("g")
                             .attr("transform", "translate(" +
                                   this._options.dimensions.margin.left +
                                   ",0)");
            d3.select(self.frameElement).style("height",
                      this._options.dimensions.height + "px");

            this._graph = new jsnx.DiGraph();
            return this;
        },

        options: function() {
            /* return current configured options */

            return this._options;
        },

        optimize: function() {
            /* optimize the nodes and dependencies of the dendrogram */

            var self = this;
            var rGraph = self._graph.reverse();
            charts.graph.group(self._graph, "renamed_from").forEach(function(source, targets) {
                targets.forEach(function (target) {
                    self._graph.edges(source).forEach(function(edge) {
                        self._graph.addEdge(target.name, edge[1]);
                    });
                    rGraph.edges(source).forEach(function(edge) {
                        self._graph.addEdge(edge[1], target.name);
                    });
                });
            });

            var weightMaps = charts.graph.map(self._graph, "weight");
            var weightFilter = function(node) {
                return weightMaps.has(node) && weightMaps.get(node) > 0;
            };
            var levelOfElements = charts.graph.group(self._graph, "weight");
            var levelOfWeights = levelOfElements.keys();
            var parentNodes = charts.graph.map(self._graph, "parent");

            var usedNodes = d3.map([]);
            var dependencies = d3.map([]);
            charts.graph.traverse(dependencies,
                                  levelOfElements.get(_.first(levelOfWeights)),
                                  self._graph,
                                  usedNodes,
                                  parentNodes,
                                  function(path) { return path.filter(weightFilter); });
            charts.graph.traverse(dependencies,
                                  levelOfElements.get(_.last(levelOfWeights)),
                                  self._graph.reverse(),
                                  usedNodes,
                                  parentNodes,
                                  function(path) { return path.filter(weightFilter).reverse(); });

            self._elements = levelOfElements.values().map(function(levels) {
                var valid = _.intersection(levels.map(function(node) { return node.name; }),
                                           usedNodes.keys());
                var parents = [];
                return levels.filter(function(node) { return _.indexOf(valid, node.name) > -1; })
                             .map(function(node) {
                                 var flattened = node['weight'] === parseInt(_.last(levelOfWeights));
                                 node['flattened'] = flattened;
                                 node['class'] = node['category'] ? node['category'] : 'table';
                                 if (flattened &&
                                     parentNodes.has(node.name)) {
                                     var pName = parentNodes.get(node.name);
                                     if (!usedNodes.has(pName)) {
                                         parents.push({
                                             'name': pName,
                                             'flattened': false,
                                             'category': 'report',
                                             'class': 'report'
                                         });
                                         usedNodes.set(parentNodes.get(node.name), 1);
                                     }
                                 }
                                 return node
                             })
                             .concat(parents)
                             .sort(function(a, b) {
                                 return d3.ascending(a.name, b.name);
                             });
            }).filter(function(levels) { return levels.length > 0; });

            self._flattened_elements = self._elements.reduce(function(p, c, i, a) {
                return p.concat(c);
            }, []);
            self._dependencies = dependencies.values();

            //delete self._graph;
            self.layout(self._elements, self._dependencies, false, true);
        },

        layout: function(elements, dependencies, flattened, refresh) {
            /* layout the nodes of the dendrogram */

            var self = this;

            self._nodes = charts.data.layout(elements,
                                             dependencies,
                                             self._options.dimensions,
                                             flattened);
            self._links = self._d3.cluster.links(self._nodes).filter(function(l) {
                return l.source && l.target;
            });

            if (refresh) {
                self._options.dimensions.width = self._elements.length * 300;
                self._options.dimensions.height = self._elements[self._elements.length - 1].length * 17.5;

                $('svg').attr('width', self._options.dimensions.width);
                $('svg').attr('height', self._options.dimensions.height);
            }

            self._d3.links = self._d3.svg.selectAll(".link")
                                         .data(self._links, charts.data.linkkey);

            self._d3.nodes = self._d3.svg.selectAll(".node")
                                         .data(self._nodes, charts.data.nodekey);

            self.paint(refresh);
        },

        reset: function() {
            /* reset the data loaded in the dendrogram */

            this._elements = [];
            this._dependencies = [];
            this._graph = new jsnx.DiGraph();
        },

        data: function(key) {
            /* get the data loaded in the dendrogram */

            return {'elements': this._elements,
                    'flattened_elements': this._flattened_elements,
                    'dependencies': this._dependencies}[key];
        },

        load: function(elements) {
            /* load data to the dendrogram */

            var self = this;
            elements.nodes.forEach(function(node) {
                self._graph.addNode(node.name, node);
            });
            self._graph.addEdgesFrom(elements.links.map(function (link) {
                return [link.source, link.target];
            }));

            self._elements = charts.graph.group(self._graph, 'weight').values();

            self._dependencies = self._dependencies ?
                                 _.uniq(self._dependencies.concat(elements.links)) :
                                 elements.links;

            self.layout(self._elements, self._dependencies, true, false);
            return self._graph.nodes().length;
        },

        filter: function(names, direction) {
            /* filter the nodes of the dendrogram */

            var l, n;
            if (names && names.length > 0) {
                n = charts.data.filter(names,
                                       this._elements,
                                       this._dependencies,
                                       direction);
            } else {
                n = this._elements;
            }

            this.layout(n, this._dependencies, true, true);
        },

        highlight: function(d, toggle) {
            /* highlight the node and its dependencies of the dendrogram */

            var self = this;
            charts.data.cascade(d.name, self._dependencies, false).map(function(n) {
                return _.find(self._flattened_elements, function(node) {
                    return node.name === n;
                })
            }).forEach(function(n) {
                if (n) {
                    self._d3.svg.selectAll("path.link.target-" + charts.data.nodekey(n))
                                .classed("target", toggle)
                                .each(self.updateNodes("source", toggle));
                }
            });

            charts.data.cascade(d.name, self._dependencies, true).map(function(n) {
                return _.find(self._flattened_elements, function(node) {
                    return node.name === n;
                })
            }).forEach(function(n) {
                if (n) {
                    self._d3.svg.selectAll("path.link.source-" + charts.data.nodekey(n))
                                .classed("source", toggle)
                                .each(self.updateNodes("target", toggle));
                }
            });
        },

        paint: function(transition) {
            /* paint the dendrogram */

            var self = this;
            self._d3.links.exit().remove();
            self._d3.links.enter().append("path")
                          .attr("class", function(d) {
                              return "link " + ("source-" + charts.data.nodekey(d.source)) +
                                     " " + ("target-" + charts.data.nodekey(d.target));});

            self._d3.links.attr("stroke-width", 0)
                          .attr("transform", null)
                          .attr("d", this._d3.diagonal)
                          .transition()
                          .delay(transition ? 250 : 5)
                          .each("start", function(d) {
                              d3.select(this).attr("stroke-width", 0.25);
                          })
                          .attr("stroke-width", 1.5);

            self._d3.nodes.exit().remove();
            self._d3.nodes.enter().append("g")
                          .attr("id", function(d) {
                              return "node-" + charts.data.nodekey(d);
                          })
                          .attr("class", function(d) {
                              return (d.rightEnd || d.leftEnd) ?
                                     'node at-end' : 'node';
                          })
                          .on("mouseover", function(d) {
                              self.highlight(d, true);
                          })
                          .on("mouseout", function(d) {
                              self.highlight(d, false);
                          })
                          .each(function() {
                              var d = this.__data__;
                              d3.select(this)
                                .append("circle")
                                .attr("r", 5);
                              d3.select(this).append("rect")
                                .attr('rx', 3)
                                .attr('ry', 3)
                                .attr("x", function(d) {
                                    return d.rightEnd ? 8 : ((d.leftEnd ? -6.5 : -5.3) * d.category.length - 8);
                                })
                                .attr("y", -15)
                                .attr("width", function(d) {
                                    return ((d.rightEnd || d.leftEnd) ? 6.5 : 5.3) * d.category.length;
                                })
                                .attr("height", 11)
                                .classed('category', true)
                                .classed(d.class, true);
                              d3.select(this).append("text")
                                .attr("dx", function(d) { return d.rightEnd ? 8 : -8; })
                                .attr("dy", 3)
                                .style("text-anchor", function(d) {
                                    return d.rightEnd ? "start" : "end";
                                })
                                .text(function(d) {
                                    return "";
                                })
                                .style("opacity", 1)
                                .each(function() {
                                    var d = this.__data__;

                                    var txt = d3.select(this);
                                    txt.append("tspan")
                                       .attr("x", d.rightEnd ? 3 : d.leftEnd ? -3 : -1)
                                       .attr("y", -10)
                                       .text(d.category)
                                       .classed('category', true)
                                       .classed(d.class, true);
                                    txt.append("tspan")
                                       .attr("x", d.rightEnd ? 8 : -8)
                                       .attr("y", (d.rightEnd || d.leftEnd) ? 6 : 8)
                                       .text(d.displayName);
                                });

                          });

            self._d3.nodes.attr("transform", null)
                          .transition()
                          .delay(transition ? 250 : 5)
                          .attr("transform", function(d) {
                              return "translate(" + d.y + "," + d.x + ")"; });
        }
    };

    return _dendrogram.init(options);
};