Pipeline Monitoring
====

Overview
====

This system aims for the monitoring of potential impacts on Tableau reports which caused by upstream data pipeline issues.

Architecture
----

This system is implemented as two parts -

* RESTful API Server
	* Analyzes the dependencies amoung Datameer, Vertica and Tableau and for external applications and JavaScript UI to access the analysis result
* JavaScript UI Client
	* A UI client for visualize of overall dependencies

Technical Stack
----
 
#### Backend
 * [Bottle](http://bottlepy.org/) on [CherryPy](http://www.cherrypy.org/)
    * for Web server and framework
 * [GitPython](https://gitpython.readthedocs.org/en/stable/)
    * for python git binding
 * [sqlparse](https://sqlparse.readthedocs.org/en/latest/)
    * for SQL parsing
 * [NetworkX](http://networkx.github.io/documentation/latest/index.html)
    * for graph data structure manipulation

#### Frontend
 * [Semantic UI](http://semantic-ui.com/)
    * for CSS template and UI component
 * [jQuery](https://jquery.com/)
    * for DOM manipulation
 * [D3.js](http://d3js.org/)
    * for data binding and presentation 
 * [underscore.js](http://underscorejs.org/)
    * for JavaScript object manipulation
 * [jsNetworkX](http://felix-kling.de/JSNetworkX/)
    * for graph data structure manipulation on browser (a JS porting of above NetworkX)
 * [lscache](https://github.com/pamelafox/lscache)
    * for cache storage manipulation in browser side

Current State
----

<img src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIj8+Cjxzdmcgd2lkdGg9IjY0MCIgaGVpZ2h0PSI0ODAiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6c3ZnPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CiA8Zz4KICA8dGl0bGU+TGF5ZXIgMTwvdGl0bGU+CiAgPGcgc3Ryb2tlPSJudWxsIiBpZD0ic3ZnXzEiPgogICA8ZyBzdHJva2U9Im51bGwiIGlkPSJzdmdfMiIgY2xhc3M9Im91dHB1dCI+CiAgICA8ZyBzdHJva2U9Im51bGwiIGlkPSJzdmdfMyIgY2xhc3M9ImNsdXN0ZXJzIi8+CiAgICA8ZyBzdHJva2U9Im51bGwiIGlkPSJzdmdfNCIgY2xhc3M9ImVkZ2VQYXRocyI+CiAgICAgPGcgc3Ryb2tlPSJudWxsIiBpZD0ic3ZnXzUiIGNsYXNzPSJlZGdlUGF0aCI+CiAgICAgIDxwYXRoIHN0cm9rZT0iIzMzMyIgaWQ9InN2Z182IiBzdHJva2Utd2lkdGg9IjNweCIgZmlsbD0ibm9uZSIgbWFya2VyLWVuZD0idXJsKCNhcnJvd2hlYWQxNzkpIiBkPSJtMjM3Ljk4MDMzMSw3OS42NjY2NzJsNTcuMjU4NDM4LDBsODAuMTkxODk1LDc0LjE4MTg3IiBjbGFzcz0icGF0aCIvPgogICAgICA8ZGVmcz4KICAgICAgIDxtYXJrZXIgb3JpZW50PSJhdXRvIiBtYXJrZXJIZWlnaHQ9IjYiIG1hcmtlcldpZHRoPSI4IiBtYXJrZXJVbml0cz0ic3Ryb2tlV2lkdGgiIHJlZlk9IjUiIHJlZlg9IjkiIHZpZXdCb3g9IjAgMCAxMCAxMCIgaWQ9ImFycm93aGVhZDE3OSI+CiAgICAgICAgPHBhdGggc3Ryb2tlPSJudWxsIiBpZD0ic3ZnXzciIGZpbGw9IiMzMzMiIGQ9Im0wLDBsMTAsNWwtMTAsNWw0LC01bC00LC01eiIvPgogICAgICAgPC9tYXJrZXI+CiAgICAgIDwvZGVmcz4KICAgICA8L2c+CiAgICAgPGcgc3Ryb2tlPSJudWxsIiBpZD0ic3ZnXzgiIGNsYXNzPSJlZGdlUGF0aCI+CiAgICAgIDxwYXRoIHN0cm9rZT0iIzMzMyIgaWQ9InN2Z185IiBzdHJva2Utd2lkdGg9IjNweCIgZmlsbD0ibm9uZSIgbWFya2VyLWVuZD0idXJsKCNhcnJvd2hlYWQxODApIiBkPSJtMjI5LjkyODM3NSwyNTguNDMwOTA4bDY1LjMxMDM5NCwwbDQ0LjczMzEyNCwwIiBjbGFzcz0icGF0aCIvPgogICAgICA8ZGVmcz4KICAgICAgIDxtYXJrZXIgb3JpZW50PSJhdXRvIiBtYXJrZXJIZWlnaHQ9IjYiIG1hcmtlcldpZHRoPSI4IiBtYXJrZXJVbml0cz0ic3Ryb2tlV2lkdGgiIHJlZlk9IjUiIHJlZlg9IjkiIHZpZXdCb3g9IjAgMCAxMCAxMCIgaWQ9ImFycm93aGVhZDE4MCI+CiAgICAgICAgPHBhdGggc3Ryb2tlPSJudWxsIiBpZD0ic3ZnXzEwIiBmaWxsPSIjMzMzIiBkPSJtMCwwbDEwLDVsLTEwLDVsNCwtNWwtNCwtNXoiLz4KICAgICAgIDwvbWFya2VyPgogICAgICA8L2RlZnM+CiAgICAgPC9nPgogICAgIDxnIHN0cm9rZT0ibnVsbCIgaWQ9InN2Z18xMSIgY2xhc3M9ImVkZ2VQYXRoIj4KICAgICAgPHBhdGggc3Ryb2tlPSIjMzMzIiBpZD0ic3ZnXzEyIiBzdHJva2Utd2lkdGg9IjNweCIgZmlsbD0ibm9uZSIgbWFya2VyLWVuZD0idXJsKCNhcnJvd2hlYWQxODEpIiBkPSJtMjUwLjUwNTYxNSw0MzcuMTk1MTI5bDQ0LjczMzE1NCwwbDgwLjE5MTg5NSwtNzQuMTgxODU0IiBjbGFzcz0icGF0aCIvPgogICAgICA8ZGVmcz4KICAgICAgIDxtYXJrZXIgb3JpZW50PSJhdXRvIiBtYXJrZXJIZWlnaHQ9IjYiIG1hcmtlcldpZHRoPSI4IiBtYXJrZXJVbml0cz0ic3Ryb2tlV2lkdGgiIHJlZlk9IjUiIHJlZlg9IjkiIHZpZXdCb3g9IjAgMCAxMCAxMCIgaWQ9ImFycm93aGVhZDE4MSI+CiAgICAgICAgPHBhdGggc3Ryb2tlPSJudWxsIiBpZD0ic3ZnXzEzIiBmaWxsPSIjMzMzIiBkPSJtMCwwbDEwLDVsLTEwLDVsNCwtNWwtNCwtNXoiLz4KICAgICAgIDwvbWFya2VyPgogICAgICA8L2RlZnM+CiAgICAgPC9nPgogICAgPC9nPgogICAgPGcgc3Ryb2tlPSJudWxsIiBpZD0ic3ZnXzE0IiBjbGFzcz0iZWRnZUxhYmVscyI+CiAgICAgPGcgc3Ryb2tlPSJudWxsIiBpZD0ic3ZnXzE1IiBjbGFzcz0iZWRnZUxhYmVsIj4KICAgICAgPGcgc3Ryb2tlPSJudWxsIiBpZD0ic3ZnXzE2IiBjbGFzcz0ibGFiZWwiPgogICAgICAgPGZvcmVpZ25PYmplY3QgeD0iMCIgeT0iMCIgaGVpZ2h0PSIwIiB3aWR0aD0iMCIvPgogICAgICA8L2c+CiAgICAgPC9nPgogICAgIDxnIHN0cm9rZT0ibnVsbCIgaWQ9InN2Z18xNyIgY2xhc3M9ImVkZ2VMYWJlbCI+CiAgICAgIDxnIHN0cm9rZT0ibnVsbCIgaWQ9InN2Z18xOCIgY2xhc3M9ImxhYmVsIj4KICAgICAgIDxmb3JlaWduT2JqZWN0IHg9IjAiIHk9IjAiIGhlaWdodD0iMCIgd2lkdGg9IjAiLz4KICAgICAgPC9nPgogICAgIDwvZz4KICAgICA8ZyBzdHJva2U9Im51bGwiIGlkPSJzdmdfMTkiIGNsYXNzPSJlZGdlTGFiZWwiPgogICAgICA8ZyBzdHJva2U9Im51bGwiIGlkPSJzdmdfMjAiIGNsYXNzPSJsYWJlbCI+CiAgICAgICA8Zm9yZWlnbk9iamVjdCB4PSIwIiB5PSIwIiBoZWlnaHQ9IjAiIHdpZHRoPSIwIi8+CiAgICAgIDwvZz4KICAgICA8L2c+CiAgICA8L2c+CiAgICA8ZyBzdHJva2U9Im51bGwiIGlkPSJzdmdfMjEiIGNsYXNzPSJub2RlcyI+CiAgICAgPGcgc3Ryb2tlPSJudWxsIiBpZD0iQSIgY2xhc3M9Im5vZGUiPgogICAgICA8cmVjdCBzdHJva2U9IiM2NjYiIGlkPSJzdmdfMjIiIGZpbGw9IiNlYWVhZWEiIHN0cm9rZS13aWR0aD0iMi41cHgiIGhlaWdodD0iODEuNjA5NzYiIHdpZHRoPSIxODkuNjY4NTQiIHk9IjM4Ljg2MTc5IiB4PSI0OC4zMTE4IiByeT0iNSIgcng9IjUiLz4KICAgICAgPGcgc3Ryb2tlPSJudWxsIiBpZD0ic3ZnXzIzIiBjbGFzcz0ibGFiZWwiLz4KICAgICA8L2c+CiAgICAgPGcgc3Ryb2tlPSJudWxsIiBpZD0iRCIgY2xhc3M9Im5vZGUiPgogICAgICA8Y2lyY2xlIHN0cm9rZT0iIzY2NiIgaWQ9InN2Z18yNSIgY3g9IjQ4OS40ODU5NSIgY3k9IjI2MS40MzA5IiBmaWxsPSIjRTE5ODYyIiBzdHJva2Utd2lkdGg9IjNweCIgcj0iMTQ4LjUxNDA0Ii8+CiAgICAgIDx0ZXh0IHhtbDpzcGFjZT0icHJlc2VydmUiIHRleHQtYW5jaG9yPSJtaWRkbGUiIGZvbnQtZmFtaWx5PSJNb25vc3BhY2UiIGZvbnQtc2l6ZT0iMjQiIGlkPSJzdmdfMzUiIHk9IjI2OSIgeD0iNDkxIiBzdHJva2UtbGluZWNhcD0ibnVsbCIgc3Ryb2tlLWxpbmVqb2luPSJudWxsIiBzdHJva2UtZGFzaGFycmF5PSJudWxsIiBzdHJva2Utd2lkdGg9IjAiIHN0cm9rZT0iIzAwMCIgZmlsbD0iIzAwMDAwMCI+CiAgICAgICA8dHNwYW4geT0iMjYwIiB4PSI1NTUiIGlkPSJzdmdfMjQiPlBpcGVsaW5lPC90c3Bhbj4KICAgICAgIDx0c3BhbiB5PSIyODUiIHg9IjU0NSIgaWQ9InN2Z18yNiI+TW9uaXRvcmluZzwvdHNwYW4+CiAgICAgIDwvdGV4dD4KICAgICA8L2c+CiAgICAgPGcgc3Ryb2tlPSJudWxsIiBpZD0iQiIgY2xhc3M9Im5vZGUiPgogICAgICA8cmVjdCBzdHJva2U9IiM2NjYiIGlkPSJzdmdfMjgiIGZpbGw9IiNlYWVhZWEiIHN0cm9rZS13aWR0aD0iMi41cHgiIGhlaWdodD0iODEuNjA5NzYiIHdpZHRoPSIxNzMuNTY0NjEiIHk9IjIxNy42MjYwMiIgeD0iNTYuMzYzNzYiIHJ5PSI1IiByeD0iNSIvPgogICAgIDwvZz4KICAgICA8ZyBzdHJva2U9Im51bGwiIGlkPSJDIiBjbGFzcz0ibm9kZSI+CiAgICAgIDxyZWN0IHN0cm9rZT0iI0ZGMDAwMCIgaWQ9InN2Z18zMSIgZmlsbD0iI2NjZiIgc3Ryb2tlLXdpZHRoPSIyLjVweCIgc3Ryb2tlLWRhc2hhcnJheT0iNSIgaGVpZ2h0PSI4MS42MDk3NiIgd2lkdGg9IjIxNC43MTkxIiB5PSIzOTYuMzkwMjUiIHg9IjM1Ljc4NjUyIiByeT0iNSIgcng9IjUiLz4KICAgICAgPGcgc3Ryb2tlPSJudWxsIiBpZD0ic3ZnXzMyIiBjbGFzcz0ibGFiZWwiPgogICAgICAgPGcgc3Ryb2tlPSJudWxsIiBpZD0ic3ZnXzMzIi8+CiAgICAgIDwvZz4KICAgICA8L2c+CiAgICA8L2c+CiAgIDwvZz4KICA8L2c+CiAgPHRleHQgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIgdGV4dC1hbmNob3I9Im1pZGRsZSIgZm9udC1mYW1pbHk9Ik1vbm9zcGFjZSIgZm9udC1zaXplPSIyNCIgaWQ9InN2Z18zNiIgeT0iODciIHg9IjE0MiIgc3Ryb2tlLWxpbmVjYXA9Im51bGwiIHN0cm9rZS1saW5lam9pbj0ibnVsbCIgc3Ryb2tlLWRhc2hhcnJheT0ibnVsbCIgc3Ryb2tlLXdpZHRoPSIwIiBzdHJva2U9Im51bGwiIGZpbGw9IiMwMDAwMDAiPmJpX2FuYWx5dGljczwvdGV4dD4KICA8dGV4dCB4bWw6c3BhY2U9InByZXNlcnZlIiB0ZXh0LWFuY2hvcj0ibWlkZGxlIiBmb250LWZhbWlseT0iTW9ub3NwYWNlIiBmb250LXNpemU9IjI0IiBpZD0ic3ZnXzM3IiB5PSIyNjYiIHg9IjE0MCIgc3Ryb2tlLWxpbmVjYXA9Im51bGwiIHN0cm9rZS1saW5lam9pbj0ibnVsbCIgc3Ryb2tlLWRhc2hhcnJheT0ibnVsbCIgc3Ryb2tlLXdpZHRoPSIwIiBzdHJva2U9Im51bGwiIGZpbGw9IiMwMDAwMDAiPmJpX3RhYmxlYXU8L3RleHQ+CiAgPHRleHQgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIgdGV4dC1hbmNob3I9Im1pZGRsZSIgZm9udC1mYW1pbHk9Ik1vbm9zcGFjZSIgZm9udC1zaXplPSIyNCIgaWQ9InN2Z18zOCIgeT0iNDQ0IiB4PSIxNDEiIHN0cm9rZS1saW5lY2FwPSJudWxsIiBzdHJva2UtbGluZWpvaW49Im51bGwiIHN0cm9rZS1kYXNoYXJyYXk9Im51bGwiIHN0cm9rZS13aWR0aD0iMCIgc3Ryb2tlPSJudWxsIiBmaWxsPSIjMDAwMDAwIj5BbGFybXM8L3RleHQ+CiA8L2c+Cjwvc3ZnPg==">

The system uses below two repositories for the analysis of existent data pipelines dependencies. But still not yet 100% covered and accurate.

* [bi_analytics](https://gitlab.addsrv.com/data-platform/bi_analytics) 
	* For resolving Datameer export jobs and Vertica data migration scripts
		* On Datameer end, still lack of *datacube* related information
		* On Vertica end, there're just too many migration scripts to identify the most up to date information
* [bi_tableau](https://gitlab.addsrv.com/data-platform/bi_tableau) 
	* For resolving Tableau report queries
		* Since the published names could be different from the name stored in the workbook/worksheet/datasource, there will be some manually works required to align the name  used on both sides.   

For the next step, this system shall integrate the pipeline health check mechanisms (any existents or to be created) as one of the input to visualize the status on the UI.


Test
----

 Run below command to perform tests
 
`py.test tests/`


Execution
----

#### Local Instance

 Please install all python module dependencies by below command -
 
`pip install -r requirements.txt`

 Then the server can be launched by below command and the server will be listening on port 8484 by default -
 
`PYTHONPATH=. python tabdash/server.py`

 Finally, the [dendrogram](http://localhost:8484/monitoring.html) will be available at -
 
`http://localhost:8484/monitoring.html`

#### Configurations

 All configurations are stored in `tabdash.cfg`, available options are -
 
* __Web Server__ Section
    * __server__: `cherrypy` as default, the web server backend of the API server
    * __host__: `0.0.0.0` as default, the host of the API server
    * __port__: `8484` as default, the port of the API server
* __Cache__ Section
    * __size__: `4096` as default, the maximum bytes of the cache
    * __ttl__: `86400` as default, the maximum seconds
* __Resolver__ Section
    * __workers__: `10` as default, the maximun workers to be used for document resolution
    * __logging__: `INFO` as default, the logging level of the resolvers
    * __debug__: `False` as default, whether to ture on in debug mode
    * __refresh__: `True` as default, whether to refresh document root
* __Resolver/Datameer__ Section
    * __root__: the root path of Datameer related files
    * __files__: the matching pattern of the files to be resolved
* __Resolver/Vertica__ Section
    * __root__: the root path of Vertica related files
    * __files__: the matching pattern of the files to be resolved
* __Resolver/Tableau__ Section
    * __root__: the root path of Tableau related files
    * __files__: the matching pattern of the files to be resolved


Try
----

A running demo of can be reached [here](http://bistage-2-001.bi.qa.dal2.mz-inc.com:8484/monitoring.html).

