#!/usr/bin/env python2.7
"""
Copyright Addmired Inc 2013. All rights reserved

"""
import argparse
import sys
import os
import os.path

if __name__ == '__main__' and sys.path[0]:
    sys.path.append(sys.path[0].rsplit('/', 1)[0])

from utils import ConfigLoader, TimeUtil, Vertica, EmailUtil, GameUtil

def main():
    parser = argparse.ArgumentParser(prog=sys.argv[0], add_help=True)
    parser.add_argument('-g', '--game', default='ody')
    parser.add_argument('-e', '--env', default='local')

    # retrieve the arguments
    args = vars(parser.parse_args(sys.argv[1:]))
    game = args['game']
    env = args['env']

    # get the config
    config_loader = ConfigLoader(game, env, 'tabdash')
    config = config_loader.getConfig()

    current_ts = TimeUtil.getCurrentTimestamp()
    current_time = TimeUtil.ts2str(current_ts)

    print("Game: {game}".format(game=game))
    print("Env: {env}".format(env=env))
    print("Current ts: {current_ts}".format(current_ts=current_ts))
    print("Current time: {current_time}".format(current_time=current_time))
    
    # send the email out
    message = "test message"
    recipients = config['email']['to_address']
    subject = "Load TV Ads CTI [{} UTC]".format(current_time)
    EmailUtil.send_email(recipients, subject, message)
    sys.exit(0)

if __name__ == '__main__':
    main()

