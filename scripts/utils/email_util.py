#!/usr/bin/env python2.7

from email.mime.text import MIMEText
from subprocess import Popen, PIPE

class EmailUtil(object):

    DEFAULT_SENDER = 'no-reply@vertica-report.machinezone.com'

    @staticmethod
    def send_email(recipients, subject, content, sender=None):
        if sender is None or sender == '':
            sender = EmailUtil.DEFAULT_SENDER
        msg = MIMEText(content)
        msg["From"] = sender
        msg["To"] = recipients
        msg["Subject"] = subject
        p = Popen(["/usr/sbin/sendmail", "-t"], stdin=PIPE)
        p.communicate(msg.as_string())