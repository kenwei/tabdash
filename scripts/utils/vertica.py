#!/usr/bin/env python2.7
"""
Copyright Machine Zone Inc 2014. All rights reserved

"""
from os.path import dirname as os_dirname, abspath as os_abspath
import subprocess

class Vertica:
    
    @staticmethod
    def execute_sql_file(settings, sql, sql_file, output=None, delim=','):
        if output is not None:
            open(output, 'w').close()

        with open(sql_file, 'w') as sql_file_output:
            sql_file_output.write(sql)
        if output is not None:
            cmd = "{vsql} -U {db_user} -w \"{db_pass}\" -h {db_host} -f {sql_file} -A -F'{delim}' -t | grep -v '([0-9]* rows)' >> {output}".format(
                    vsql=settings['vsql_path'],
                    db_user=settings['db_user'],
                    db_pass=settings['db_pass'],
                    db_host=settings['db_host'],
                    sql_file=sql_file,
                    delim=delim,
                    output=output)
        else:
            cmd = "{vsql} -U {db_user} -w \"{db_pass}\" -h {db_host} -f {sql_file}".format(
                    vsql=settings['vsql_path'],
                    db_user=settings['db_user'],
                    db_pass=settings['db_pass'],
                    db_host=settings['db_host'],
                    sql_file=sql_file)
        ps = subprocess.Popen(cmd,shell=True,stdout=subprocess.PIPE,stderr=subprocess.STDOUT)
        ps.communicate()

    @staticmethod
    def load_sql_template(template, feature='tvads'):
        sql_template_dir = os_dirname(os_abspath(__file__)).replace('utils', 'sql_template')
        template_file = "%s/%s/%s.tpl.sql" % (sql_template_dir, feature, template)
        with open(template_file) as f:
            return f.read()