#!/usr/bin/env python2.7

class GameUtil(object):

    DEFAULT_GAME = 'ody'
    DEFAULT_GAME_ID = 15

    @staticmethod
    def get_dim_game():
        return {'ody': 15, 'wiso': 19}

    @staticmethod
    def get_game_id(game):
        game_id = GameUtil.DEFAULT_GAME_ID
        dim_game = GameUtil.get_dim_game()
        if game in dim_game:
            game_id = dim_game[game]
        return game_id
