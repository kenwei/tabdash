#!/usr/bin/env python2.7
"""
Copyright Machine Zone Inc 2013. All rights reserved

"""
from os.path import dirname as os_dirname, abspath as os_abspath
from ConfigParser import SafeConfigParser

class ConfigLoader(object):
    def __init__(self, game='ody', env='local', feature='global'):
        self.config = self.loadConfig(game, env, feature)

    def loadConfig(self, game='ody', env='local', feature='global'):
        filename = 'cfg/{}/{}.{}.cfg'.format(feature, game, env)
        config_file = os_dirname(os_abspath(__file__)).replace('utils', filename)

        config_parser = SafeConfigParser()
        config_parser.read(config_file)
        config = {}
        sections = config_parser.sections()
        for section in sections:
            config[section] = {}
            options = config_parser.options(section)
            for option in options:
                try:
                    config[section][option] = config_parser.get(section, option)
                except:
                    config[section][option] = None
        return config

    def getConfig(self):
        return self.config