#!/usr/bin/env python2.7
"""
Copyright Machine Zone Inc 2013. All rights reserved

"""
import calendar
import time
import datetime

class TimeUtil(object):
    DAY_IN_SECOND = 86400
    HOUR_IN_SECOND = 3600
    HOUR_IN_MINUTE = 60
    MINUTE_IN_SECOND = 60

    @staticmethod
    def getCurrentTimestamp():
        return time.time()

    @staticmethod        
    def getCurrentTime():
        current_ts = TimeUtil.getCurrentTimestamp()
        return TimeUtil.ts2str(current_ts)
    
    @staticmethod        
    def getCurrentHour():
        current_ts = TimeUtil.getCurrentTimestamp()
        return TimeUtil.getHour(current_ts)

    @staticmethod
    def getPreviousHour():
        current_ts = TimeUtil.getCurrentTimestamp()
        previous_hour_ts = (int(current_ts / TimeUtil.HOUR_IN_SECOND) - 1) * TimeUtil.HOUR_IN_SECOND
        return TimeUtil.getHour(previous_hour_ts)

    @staticmethod        
    def getTodayStartTimestamp():
        current_ts = TimeUtil.getCurrentTimestamp()
        return int(current_ts / TimeUtil.DAY_IN_SECOND) * TimeUtil.DAY_IN_SECOND

    @staticmethod
    def getTimestampFromUTC(date):
        return calendar.timegm(datetime.datetime.strptime(date, "%Y-%m-%d").timetuple())

    @staticmethod
    def getTimestampFromUTCTime(utc_time):
        return calendar.timegm(datetime.datetime.strptime(utc_time, "%Y-%m-%d %H:%M:%S").timetuple())

    @staticmethod
    def getTodayDate():
        current_ts = TimeUtil.getCurrentTimestamp()
        return TimeUtil.getDate(current_ts)

    @staticmethod
    def getDate(ts):
        return datetime.datetime.utcfromtimestamp(ts).strftime('%Y-%m-%d')        

    @staticmethod
    def getHour(ts):
        return datetime.datetime.utcfromtimestamp(ts).strftime('%Y-%m-%d %H') + ':00:00'

    @staticmethod
    def ts2str(ts):
        return datetime.datetime.utcfromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')

    @staticmethod
    def getTimePeriod(seconds):
        return str(datetime.timedelta(seconds))

    @staticmethod
    def getDateKey(ts):
        return datetime.datetime.utcfromtimestamp(ts).strftime('%Y%m%d')

    @staticmethod
    def extractDateKey(date_str):
        parts = date_str.split(' ')
        return parts[0].replace('-', '')

    @staticmethod
    def sleep(seconds):
        time.sleep(seconds)