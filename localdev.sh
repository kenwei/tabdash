#!/usr/bin/env bash
BASEDIR=$(dirname $0)

cd $BASEDIR

if [ ! -f orientdb/data.json ]; then
    echo "To generate the data file for orientdb initiation"
	PYTHONPATH=. python tabdash/extract.py
else echo "CHECKED: 'data.json' exists"
fi

if [[ $(docker-machine ls | awk 'BEGIN{FS=" "} $1~/dev/ {print $1}') != *dev* ]]; then
	echo "To create 'dev' docker host"
	docker-machine create --driver virtualbox dev
else echo "CHECKED: 'dev' docker host exists"
fi
if [[ $(docker-machine ls | awk 'BEGIN{FS=" "} $1~/dev/ {print $4}') != *Running* ]]; then
	echo "To run 'dev' docker host"
	docker-machine start dev
else echo "CHECKED: 'dev' docker host is running"
fi

eval "$(docker-machine env dev)"

if [[ $(docker images | awk 'BEGIN{FS=" "} $1~/orientdb/ {print $1}') != *orientdb* ]]; then
	echo "To pull and run 'orientdb' docker image"
	cd orientdb
	docker build -t orientdb .
	cd ..
	docker -D run --name=orientdb --hostname=orientdb -p 2424:2424 -p 2480:2480 -d orientdb
else echo "CHECKED: 'orientdb' docker image exists"
fi

if [[ $(docker ps | awk 'BEGIN{FS=" "} $NF~/redis/ {print $NF}') != *redis* ]]; then
	echo "To start 'orientdb' docker container"
	docker start orientdb
else echo "CHECKED: 'orientdb' docker container started"
fi

