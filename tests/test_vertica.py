import logging
import os
from os.path import join as op_join

from concurrent.futures import ThreadPoolExecutor

import tabdash
from tabdash.resolver.vertica import PipelineDependencyResolver


class TestPipelineDependencyResolver:

    def test_resolve(self):
        logging.basicConfig(level=logging.DEBUG)
        doc_root = op_join(os.path.dirname(tabdash.__file__),
                           '..', 'dependencies', 'bi_analytics')

        cache = {}
        executor = ThreadPoolExecutor(max_workers=5).map
        resolver = PipelineDependencyResolver(
            {'root': doc_root,
             'files': '*/vertica/views.sql'},
            cache=cache)
        for resolution in resolver.resolve(executor=executor):
            for graphs in resolution:
                assert graphs.has_node('mk_tableau.dw_cti_vw')
                assert graphs.has_edge('mk_tableau.dw_install_fct',
                                       'mk_tableau.dw_cti_vw')
                assert graphs.has_edge('mk_tableau.dw_click_fct',
                                       'mk_tableau.dw_cti_vw')

        assert len(cache.keys()) > 0
