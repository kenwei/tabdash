import logging

import os
from os.path import join as op_join

from concurrent.futures import ThreadPoolExecutor

import tabdash
from tabdash.resolver.datameer import DataMeerDependencyResolver


class TestDataMeerDependencyResolver:

    def test_resolve(self):
        logging.basicConfig(level=logging.DEBUG)
        doc_root = op_join(os.path.dirname(tabdash.__file__),
                           '..', 'dependencies', 'bi_analytics')
        cache = {}
        executor = ThreadPoolExecutor(max_workers=5).map
        resolver = DataMeerDependencyResolver(
            {'root': doc_root,
             'files': '*/roi/roi_worker.py, '
             '*/roi/exportjob/*.json'},
            cache=cache)
        for resolution in resolver.resolve(executor=executor):
            for graphs in resolution:
                assert graphs is not None

        assert len(cache.keys()) > 0
