from ConfigParser import ConfigParser
import logging
import os
from os.path import join as op_join

import tabdash
from tabdash.resolver.resolver import DependencyResolver


class TestDependencyResolver:

    def test_resolve(self):
        config = ConfigParser()
        config.add_section('Resolver')
        config.set('Resolver', 'refresh', 'False')
        config.set('Resolver', 'debug', 'True')
        config.set('Resolver', 'workers', '1')

        config.add_section('Resolver/Datameer')
        config.set('Resolver/Datameer', 'root',
                   op_join(os.path.dirname(tabdash.__file__),
                           '..', 'dependencies', 'bi_analytics'))
        config.set('Resolver/Datameer', 'files', '*/roi/roi_worker.py')

        config.add_section('Resolver/Vertica')
        config.set('Resolver/Vertica', 'root',
                   op_join(os.path.dirname(tabdash.__file__),
                           '..', 'dependencies', 'bi_analytics'))
        config.set('Resolver/Vertica', 'files', '*/vertica/views.sql')

        config.add_section('Resolver/Tableau')
        config.set('Resolver/Tableau', 'root',
                   op_join(os.path.dirname(tabdash.__file__),
                           '..', 'dependencies', 'bi_tableau'))
        config.set('Resolver/Tableau', 'files', '*/gow_kpi_hourly_14d.twb')

        logging.basicConfig(level=logging.DEBUG)

        for resolutions in DependencyResolver(config).resolve():
            for graph in resolutions:
                assert graph is not None
