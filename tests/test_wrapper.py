import os
try:
    import ujson as json
except:
    import json

from networkx.readwrite import json_graph

import tabdash
from tabdash.graph.wrapper import DependencyGraphWrapper


class TestDependencyGraphWrapper:

    def test_get_edges(self):
        sample_file = os.path.join(os.path.dirname(tabdash.__file__),
                                   '..', 'web', 'json', 'sample.json')
        sample_data = ''.join(open(sample_file).readlines())
        graph = json_graph.node_link_graph(json.loads(sample_data))
        cache = {'dendrogram': graph}
        wrapper = DependencyGraphWrapper(None, cache=cache)
        assert len(wrapper.get_edges('roi_p2.v_user_first')) > 0
