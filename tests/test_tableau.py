import os
from os.path import join as op_join
import logging

from concurrent.futures import ThreadPoolExecutor

import tabdash
from tabdash.resolver.tableau import TableauDependencyResolver


class TestTableauDependencyResolver:

    def test_resolver(self):
        logging.basicConfig(level=logging.DEBUG)
        doc_root = op_join(os.path.dirname(tabdash.__file__),
                           '..', 'dependencies', 'bi_tableau')
        cache = {}
        executor = ThreadPoolExecutor(max_workers=5).map
        resolver = TableauDependencyResolver(
            {'root': doc_root,
             'files': '*/gow/*/gow_kpi_hourly_14d.twb'},
            cache=cache)
        for resolution in resolver.resolve(executor=executor):
            for graphs in resolution:
                assert graphs.has_node('datacube.hourly_report')

        assert len(cache.keys()) > 0
